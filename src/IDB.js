import babelPolyfill from 'babel-polyfill'

class IDB {
    constructor(storeName) {
        var _this = this
        this.storeName = storeName
        this.request = indexedDB.open('messages')

        this.request.onsuccess = function(ev) {
            _this.db = ev.target.result
            console.log('onSuccess db', _this.db)
        }

        this.request.onerror = function(ev) {
            console.log('onError')
        }
 
        this.request.onupgradeneeded = function(ev) {
            _this.db = ev.target.result
            var objectStore = _this.db.createObjectStore(storeName, { keyPath: 'id' })
            console.log('onUpgradeNeeded db', _this.db)
        }
    }

    saveMessage(message) {
        console.log('saveMessage', message)
        console.log('saveMessage db', this.db)
        var tx = this.db.transaction([this.storeName], 'readwrite')
        var objectStore = tx.objectStore(this.storeName)
        objectStore.put(message)
    }

    getMessages() {
        var arr = []
        var tx, objectStore, request
        try {
            tx = this.db.transaction([this.storeName], 'readonly')
            objectStore = tx.objectStore(this.storeName)
            request = objectStore.openCursor()
            request.onsuccess = async function(ev) {
                var cursor = ev.target.result
                if(cursor) {
                    await arr.push(cursor.value)
                    console.log('MESSAGE', cursor.value)
                    cursor.continue()
                }
            }
        } catch (error) {
            console.error('something wrong with database')
        }
        // if(cursor) {
        //     arr.push(cursor.value)
        //     console.log('MESSAGE', cursor.value)
        //     cursor.continue()
        // // }
        // request.onsuccess = async function(ev) {
        //     var cursor = ev.target.result
        //     if(cursor) {
        //         await arr.push(cursor.value)
        //         console.log('MESSAGE', cursor.value)
        //         cursor.continue()
        //     }
        // }
        return arr
    }

}

export default IDB