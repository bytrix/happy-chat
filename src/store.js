import Vue from 'vue'
import Vuex from 'vuex'
import VueSocketio from 'vue-socket.io'
import socketio from 'socket.io-client'
import axios from 'axios'
import router from './router'
import env from '../env/default'
import IDB from './IDB'

axios.defaults.baseURL = env.API_HOST

Vue.use(Vuex)

var state = {
    db: null,
    onlineUsers: [],
    newUser: {
        username: null,
        password: null
    },


    // chatUser: {
    //     id: 0,
    //     username: null,
    //     // messages: [
    //     //     { content: 'aa' },
    //     //     { content: 'dfsgdfg' },
    //     // ]
    // },
    // chatRoom: {
    //     id: 0,
    //     name: null
    // },
    chatWindow: {
        type: null,
        show: false,
        chatObject: null
        // type: 'ROOM'
    },


    // chatUsers: [],
    // chatUserSet: new Set(),
    // showChat: false,
    newFriend: false,
    // newFriend: {},
    user: {
        id: 0,
        username: null,
        password: null,
        friends: [],
        new_friends: []
    },
    message: {
        from_u: { id: 0 },
        /*
        to_u: null,
        content: '',
        date: null,
        read: false
        */
    },
    // messages: [],

    // messages: [
    //     {
    //         from_u: { id: 2, username: 'aa' },
    //         to_u: { id: 1, username: 'jj' },
    //         content: 'm1',
    //         read: false,
    //         timestamp: '2-1000'
    //     },
    //     {
    //         from_u: { id: 2, username: 'aa' },
    //         to_u: { id: 1, username: 'jj' },
    //         content: 'm2',
    //         read: false,
    //         timestamp: '2-1001'
    //     },
    //     {
    //         from_u: { id: 3, username: 'bb' },
    //         to_u: { id: 1, username: 'jj' },
    //         content: 'm3',
    //         read: false,
    //         timestamp: '3-1003'
    //     }
    // ],
    messages: [],
    // messages: JSON.parse(localStorage.getItem(sessionStorage.getItem('uid'))) || [],
    // messages: function() {
    //     var arr = []
    //     for(var i = 0; i < localStorage.length; i++) {
    //         var k = localStorage.key(i)
    //         // U1517669987015-1-3 从LocalStorage中检索类似的消息编号
    //         if(/[UR]\d{13}-\d?-\d?/.test(k)) {
    //             var message = JSON.parse(localStorage.getItem(k))
    //             arr.push(message)
    //         }
    //     }
    //     return arr
    // }(),


    // messageSet: new Set(),
    sidebar: true,
    drawer: true,
    snackbar: {},   //下方通知
    notifySnackbar: {}, //右上角通知
    // loaded: false,
    chatFooter: {
        show: true
    },
    RTC: {
        peer: null,
        peerConnection: new RTCPeerConnection(),
        dataChannel: null,
        connected: false,
        videoStatus: 'DISCONECTED',     // DUDUDU, CONNECTED
        fileTransfer: {
            tempFile: null,
            accept: false
        }
    },
    // receivedFile: null,
    filePackets: [],
    // downloadingFile: {
    //     name: null,
    //     size: 0,
    //     data: '',
    //     currentPacket: 0,
    //     totalPacket: 0,
    //     complete: false
    // },
    downloadingFile: null,
    // sharedFiles: [],
    // sharedFiles: {
    //     'file_one': { name: 'file_one', size: 123, data: '12345678' },
    //     'file_two': { name: 'file_two', size: 456, data: 'abcdefg' },
    //     'file_three': { name: 'file_three', size: 789, data: 'HIJKLMN' }
    // },
    sharedFiles: {},
    // sharedFiles: [
    //     { name: 'file_1', size: 123, data: '123456789' },
    //     { name: 'file_2', size: 456, data: 'abcdefg' },
    //     { name: 'file_3', size: 789, data: 'HIJKLMN' }
    // ],
    debug: null
}

var getters = {
    isOnline(uid) {
        return uid
    }
}

var actions = {
    register() {
        this.commit('REGISTER')
    },
    login() {
        this.commit('LOGIN')
    },
    getUser() {
        this.commit('GET_USER')
    },
    // socket_receive() {
    //     alert('received');
    // }
    loadMessages() {
        this.commit('LOAD_MESSAGES')
    },
    transferFile() {
        this.commit('TRANSFER_FILE')
    }
}

var mutations = {
    SOCKET_RECEIVED() {
        alert('kkk')
    },
    REGISTER() {
        // var u = {
        //     username: state.newUser.username,
        //     password: state.newUser.password
        // }
        // if(u.username == '' || u.password == '') {
        //     state.alert.show = true
        //     state.alert.message = '用户名或密码不能为空'
        // }
        // axios.post('http://localhost:8081/users', u).then(function(response) {
        //     console.log(response.data)
        //     router.push('/login')
        // })
        // .catch(function(e) {
        //     console.log('注册失败')
        //     state.alert.show = true
        //     state.alert.message = '该账号已被注册'
        // })
    },
    LOGIN() {
        // var u = {
        //     username: state.user.username,
        //     password: state.user.password
        // }
        // if(u.username == '' || u.password == '') {
        //     // state.alert.show = true
        //     // state.alert.message = '用户名或密码不能为空'
        //     return
        // }
        // axios.post('http://localhost:8081/session', u).then(function(response) {
        //     // if(response.data.user.id == null) {
        //     //     state.alert.show = true
        //     //     state.alert.message = '用户名或密码错误'
        //     //     return
        //     // }
        //     sessionStorage.setItem('uid', response.data.user.id)
        //     // socketio('http://localhost:3001', { query: "uid="+sessionStorage.getItem('uid') })
        //     // Vue.use(VueSocketio, socketio('http://localhost:3001', { query: "uid="+sessionStorage.getItem('uid') }), this)
        //     // new Vue().$socket.emit('login', response.data.user.id)
        //     // state.user = response.data.user
        //     router.push('/')
        //     router.go(0)
        // })
        // .catch(function(e) {
        //     console.log('登录失败')
        //     // state.alert.show = true
        //     // state.alert.message = '登录失败'
        // })
    },
    GET_USER() {
        var uid = sessionStorage.getItem('uid')
        if(uid == null) {
            router.push('/login')
            return
        }
        
        axios.get('/users/' + uid).then(function(response) {
            state.user = response.data.user
            // state.loaded = true
            // console.log('+++++++++++++++++++++++++++++++')
            // console.log('friends', state.user.friends)
            
            // console.log('+++++++++++++++++++++++++++++++')
            console.debug('store', state.user)
            state.db = new IDB(uid)
            // alert('store')
        })
        .catch(function(e) {
            console.log('获取用户失败')
        })
    },
    async LOAD_MESSAGES() {
        if(state.db != null) {
            state.messages = await state.db.getMessages()
        }
    },
    TRANSFER_FILE() {
        // alert(`begin to transfer file ${state.RTC.fileTransfer.tempFile.name}`)

        var tempFile = state.RTC.fileTransfer.tempFile

        // 自己发送文件
        var packetSize = 1000
        var packetNum = Math.ceil(tempFile.data.length / packetSize)
        // console.log('readyState', state.RTC.dataChannel.readyState)
        // console.log('dataChannel ->', state.RTC.dataChannel)
        // if(state.RTC.dataChannel.readyState == 'open') {
            // state.RTC.dataChannel.send(JSON.stringify(tempFile))
        // }
        // state.RTC.dataChannel.onopen = function(event) {
            // state.sharedFiles.push(tempFile)
            // state.RTC.dataChannel.send(JSON.stringify(tempFile))
            var filePackets = []
            var type =  tempFile.data.split(';base64')[0].split('data:')[1]
            // console.log('slice', tempFile.data.split(';base64'))
            for(var i = 0; i < packetNum; i++) {
                // var filePacket = tempFile
                // console.log('i', i)
                var filePacket = {
                    // type: 'PACKET',
                    type: type,
                    name: tempFile.name,
                    size: tempFile.size,
                    data: tempFile.data.substr(i * packetSize, packetSize),
                    currentPacket: i,
                    totalPacket: packetNum,
                    complete: (i+1) >= packetNum ? true : false
                }
                // filePacket.data = tempFile.data.substr(i * packetNum, packetSize)
                // filePacket.currentPacket = i
                // filePacket.totalPacket = packetNum
                filePackets.push(filePacket)
                // state.RTC.dataChannel.send(JSON.stringify(filePacket))
            }
            var i = 0
            var timer = setInterval(function() {
                // console.log(i, filePackets[i].data)
                // console.log(i, packetNum)

                try {
                    state.downloadingFile = {
                        type: filePackets[i].type,
                        name: filePackets[i].name,
                        size: filePackets[i].size,
                        currentPacket: filePackets[i].currentPacket,
                        totalPacket: filePackets[i].totalPacket,
                        data: state.downloadingFile ? state.downloadingFile.data + filePackets[i].data : '',
                        complete: i > packetNum ? true : false,
                        accept: false
                    }
                    
                    // if(filePackets[i].currentPacket > filePackets[i].totalPacket) {
                    //     state.downloadingFile.complete = true
                    // }
                } catch(err) {
                    // alert(err)
                    clearInterval(timer)
                    // console.error(err)
                }

                // console.log(i, packetNum, i > packetNum)
                
                state.RTC.dataChannel.send(JSON.stringify(filePackets[i]), function() {
                    console.log('packet in ChatFooter.vue, send ok!', filePackets[i])
                })

                i++

                if(i > packetNum) {
                    // alert('complete, in ChatFooter.vue')
                    // state.downloadingFile.complete = true
                    state.sharedFiles[tempFile.name] = state.downloadingFile
                    state.downloadingFile = null
                    clearInterval(timer)
                }

            }, 100)
        // }


        
    }
}

export default new Vuex.Store({
    state, mutations, actions, getters
})