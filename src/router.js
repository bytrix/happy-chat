import Vue from 'vue'
import Router from 'vue-router'
import ChatApp from '@/components/ChatApp'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Debug from '@/components/Debug'
// import Loading from '@/components/Loading'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'ChatApp',
            component: ChatApp
        },
        // {
        //     path: '/loading',
        //     name: 'Loading',
        //     component: Loading
        // },
        // {
        //     path: '/u/:uid',
        //     name: 'ChatApp',
        //     component: ChatApp
        // },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/debug',
            name: 'Debug',
            component: Debug
        }
    ]
})