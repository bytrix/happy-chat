import Vue from 'vue'
import VueSocketio from 'vue-socket.io'
import socketio from 'socket.io-client'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'
import store from './store'
import 'vuetify/dist/vuetify.css'
import axios from 'axios'
// import env from '../env/default'
import IDB from './IDB'
var env = require('../env/default')

Vue.use(Vuetify)
// Vue.use(VueSocketio, socketio('http://localhost:3001'), store)
Vue.use(VueSocketio, socketio(env.SOCKET_HOST, {
    query: "uid="+sessionStorage.getItem('uid'),
    // secure: true
}), store)
// Vue.use(VueSocketio,socketio('http://localhost:3001'))
axios.defaults.baseURL = env.API_HOST
Vue.prototype.$http = axios
Vue.prototype.env = env

// Vue.prototype.$db = null

new Vue({
  data() {
    return {
      peer: null
    }
  },
  mounted() {
    var _this = this
    setTimeout(function() {
      // _this.$store.state.db.getMessages()
      if(_this.$store.state.messages.length == 0) {
        _this.$store.dispatch('loadMessages')
      }
    }, 100)
    // 由于从IndexedDB中加载数据是一个异步操作，所以这里设置计时器，过0.1秒后再加载数据

  },
  // sockets: {
  //   connected: function() {
  //     console.log('connected')
  //   },
  //   chat: function() {
  //     console.log('chat..')
  //   }
  // },
  sockets: {
    // connect: function() {
    //   alert('connect')
    // },
    debug: function(data) {
      console.log(data)
      console.log(this.$store.state.message)
      // this.$store.state.debug = this.$store.state.message
    },
    receive: function(message) {
      var _this = this
      // if(typeof message == 'string') {
      //   message = JSON.parse(message)
      // }
      // console.debug('messages', this.$store.state.messages)
      // this.$store.state.debug = this.$store.state.messages

      // if(this.$store.state.showChat && this.$store.state.chatUser.id == message.from_u.id) {
      //   message.read = true
      // }
      console.log(message)
      if(this.$store.state.chatWindow.show && this.$store.state.chatWindow.chatObject.id == message.from_u.id) {
        message.read = true
      }

      // alert('received')
      // console.log(message)
      this.$store.state.message = message
      // this.$store.state.chatUser.id = message.from_uid
      this.$store.state.messages.push(message)
      // localStorage.setItem(this.$store.state.user.id, JSON.stringify(this.$store.state.messages))
      this.$store.state.messages.forEach(function(message) {
        // localStorage.setItem(message.id, JSON.stringify(message))
        // alert(_this.$store.state.db)
        _this.$store.state.db.saveMessage(message)
      })
    },
    onInviteVideoChat: function(peer) {
      // alert(1)
      var _this = this
      console.log('onInviteVideoChat')
      console.log('peer', peer)
      this.$store.state.notifySnackbar = {
        show: true,
        message: `${peer.user.username} 邀请与你视频通话`,
        from_user: peer.user,
        timeout: 0,
        event: {
          agree: function() {
            // alert('agree!!')
            _this.$socket.emit('joinVideoChat', {
              peer: peer
            })
            _this.$store.state.notifySnackbar.show = false
            // _this.$store.state.RTC.connected = true
          }
        }
      }
    },
    receiveOffer: function(packet) {
      // if(this.$store.state.RTC.type == 'DATA_CHANNEL') {
      //   return
      // }
      var _this = this
      this.$store.state.RTC.peer = packet.peer
      console.log('收到Offer', packet)


      this.$store.state.RTC.peerConnection.onicecandidate = function(event) {
          console.log('创建candidate')
          // console.log('peer', _this.$store.state.RTC.peer)
          _this.$socket.emit('createCandidate', {
              peer: _this.$store.state.RTC.peer,
              data: event.candidate
          })
      }



      this.$store.state.RTC.peerConnection.ondatachannel = function(event) {
        // alert('ondatachannel')
          console.log('ondatachannel in main.js', event.channel)
          console.log('this.$store.state.RTC.dataChannel', _this.$store.state.RTC.dataChannel)
          // DataChannel不能放在Vuex里面，这是两个不同的channel
          // _this.$store.state.RTC.dataChannel = event.channel
          event.channel.onmessage = function(event) {
            /*
            // alert('onmessage in main.js')
            console.log('onmessage in main.js', event)
            // console.log(111)
            // console.log('event.data in main.js', event.data)
            try {
              // console.log(222)
              var json = JSON.parse(event.data)
              // console.log('json', json)
              if(json.type == 'FILE') {
                // _this.$store.state.receivedFile = json
                // _this.$store.state.receivedFiles[json.name] = json
                // _this.$store.state.receivedFiles.push(json)
                // download
                // var link = document.createElement('a')
                // link.href = json.data
                // link.download = json.name
                // var clickEvent = new MouseEvent('click')
                // link.dispatchEvent(clickEvent)
                // console.log('link', link)
                // console.log('download over!')
              }
            } catch (error) {
              console.error('Invalid JSON!')
            }
            */





            // alert('onmessage in FriendList.vue')
            console.log('onmessage in FriendList.vue', event)
            // console.log('event.data in FriendList.vue', event.data)
            // 对方接受文件
            try {
                // console.log('event.data', event.data)
                var json = JSON.parse(event.data)
                // console.log('json', json)
                // if(json.type == 'PACKET') {
                    _this.$store.state.downloadingFile = {
                        type: json.type,
                        name: json.name,
                        size: json.size,
                        currentPacket: json.currentPacket,
                        totalPacket: json.totalPacket,
                        data: _this.$store.state.downloadingFile ? _this.$store.state.downloadingFile.data + json.data : json.data,
                        complete: json.complete
                    }
                    // alert(_this.$store.state.downloadingFile)
                    // if(json.currentPacket > json.totalPacket) {
                    //     _this.$store.state.downloadingFile.complete = true
                    // }
                    if(json.complete) {
                        // alert('complete, in main.js')

                        // console.log('COMPLETE in main.js', _this.$store.state.downloadingFile.data)

                        // 自动下载文件
                        var link = document.createElement('a')
                        link.href = _this.$store.state.downloadingFile.data
                        console.log('href', link.href)
                        link.download = _this.$store.state.downloadingFile.name
                        var clickEvent = new MouseEvent('click')
                        link.dispatchEvent(clickEvent)
                        
                        _this.$store.state.sharedFiles[json.name] = json
                        _this.$store.state.downloadingFile = null
                    }
                    // console.log(_this.$store.state.downloadingFile)
                // }
            } catch (err) {
                console.error('Invalid JSON!')
            }





          }
      }
      


      this.$store.state.RTC.peerConnection.setRemoteDescription(
        new RTCSessionDescription(packet.data),


        function() {



          _this.$store.state.RTC.peerConnection.createAnswer(function(answer) {
            console.log('创建answer', answer)
            _this.$store.state.RTC.peerConnection.setLocalDescription(new RTCSessionDescription(answer))
            _this.$socket.emit('receiveAnswer', {
              peer: packet.peer,
              data: answer
            })
          }, function(error) {
            console.error(error)
          })
  
  



        }





      )
    },
    receiveAnswer: function(packet) {
        var _this = this
        console.log('收到answer', packet)
        // this.$store.state.peerConnection.onicecandidate = function(event) {
        //   console.log('onicecandidate')
        //   console.log('peer', packet.peer)
        //   _this.$socket.emit('createCandidate', {
        //     peer: packet.peer,
        //     candidate: event.candidate.candidate
        //   })
        // }
        this.$store.state.RTC.peerConnection.setRemoteDescription(new RTCSessionDescription(packet.data))
    },
    receiveCandidate: function(packet) {
      try {
        
        // var candidate = {
        //   candidate: packet.data
        // }
        // var candidate = packet.data
        var candidate = {
          candidate: packet.data.candidate
        }
        console.log('收到candidate', candidate)
        // console.log(candidate)
        this.$store.state.RTC.peerConnection.addIceCandidate(new RTCIceCandidate(candidate))
        this.$store.state.RTC.connected = true
      } catch (error) {
        console.log('HELLO?', error)
      }
    }
  },
  el: '#app',
  router,
  store,
  render: h => h(App)
})
