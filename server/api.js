var express = require('express')
var bodyParser = require('body-parser')
var knex = require('./models/knex')
var app = express()
var fs = require('fs')

var https = require('https')
var fs = require('fs')
var server = https.createServer({
    key: fs.readFileSync('./ssl/key.pem'),
    cert: fs.readFileSync('./ssl/server.crt')
}, app)

// var User = require('./models/User')
import User from './models/User'
var Room = require('./models/Room')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'))

app.get('/', function(req, res, next) {
    new User().fetch({ withRelated: ['friends'] }).then(function(user) {
        // console.log(user.toJSON())
        // console.log(user.friends().toArray())
        res.send(user.toJSON())

    })
    // res.send('ok')
})

app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1');
    next();
});

// 用户注册
app.post('/users', function(req, res, next) {
    var u = req.body
    console.log(u)
    u.join_at = new Date()
    if(u.username == '' || u.password == '') {
        res.json({
            message: '用户名或密码不能为空'
        })
        return
    }
    new User(u).save().then(function(user) {
        console.log('注册成功：' + JSON.stringify(user.toJSON()))
        // res.send(user.toJSON())
        res.status(200).json({
            message: '注册成功',
            user: user.toJSON()
        })
    })
    .catch(function(e) {
        res.status(404).json({
            message: '注册失败',
            error: e
        })
    })
    // res.send('register')
})

// 用户登录
app.post('/session', function(req, res) {
    var u = req.body
    // console.log(u)
    new User(u).fetch().then(function(user) {
        console.log('登录成功：' + JSON.stringify(user.toJSON()))
        // res.send(user.toJSON())
        res.status(200).json({
            message: '登录成功',
            user: user.toJSON()
        })
    })
    .catch(function(e) {
        // res.send(e)
        res.status(404).json({
            message: '登录失败',
            error: e
        })
    })
    // res.send('login')
})

// 通过ID获取用户
app.get('/users/:uid', function(req, res, next) {
    console.log('---')
    var _user = {}
    var u = { id: req.params.uid }
    new User(u).fetch({ withRelated: ['friends', 'rooms'] }).then(async function(user) {
        console.log('获取用户' + JSON.stringify(user.toJSON()))
        // res.send(user.toJSON())
        _user = user.toJSON()
        // console.log(user.new_friends())

        // user.new_friends().then(function(friedns) {
        //     console.log('+++++ new friends +++++')
        //     console.log(friedns)
        //     _user.new_friends = 'new friends...'
        //     console.log('+++++ new friends +++++')
        // })


        var friends = await user.new_friends()
        _user.new_friends = JSON.parse(friends)

        res.status(200).json({
            user: _user
        })
    })
    .catch(function(e) {
        res.status(404).json({
            message: '找不到该用户',
            error: e
        })
    })
})

// 通过用户名获取用户
app.get('/users', function(req, res, next) {
    var u = { username: req.query.username }
    console.log(u)
    new User(u).fetch().then(function(user) {
        // res.send(user.toJSON())
        if(user == null) {
            throw new Error('找不到该用户')
            return
        }
        res.status(200).json({
            user: user.toJSON()
        })
    })
    .catch(function(e) {
        // console.log(e)
        // res.send(null)
        res.status(404).json({
            message: '找不到该用户',
            error: e
        })
    })
})

// 添加好友
app.post('/friend', function(req, res, next) {
    var user__friend = req.body
    console.log(user__friend)
    // var uid = req.params.uid
    // var fid = req.params.fid
    // var user__friend = {
    //     user_id: uid,
    //     friend_id: fid
    // }
    // console.log(user__friend)
    var from_user = null
    var to_user = null
    knex('user__friend').insert(user__friend).then(async function(response) {
        // res.send(response)
        /*
        new User({ id: user__friend.user_id }).fetch().then(function(from_user) {
            res.status(200).json({
                message: '添加好友成功',
                from_user: from_user,
                response: response
            })
        })

        new User({ id: user__friend.friend_id }).fetch().then(function(friend) {
            res.status(200).json({
                message: '添加好友成功',
                friend: friend,
                response: response
            })
        })
        */

        from_user = await new User({ id: user__friend.user_id }).fetch()
        to_user = await new User({ id: user__friend.friend_id }).fetch()
        
        res.status(200).json({
            message: '添加好友成功',
            from_user: from_user,
            to_user: to_user,
            response: response
        })

        // console.log(r)
    })
    .catch(function(e) {
        console.log('error', e)
        // res.send(e)
        res.status(404).json({
            message: '不能重复添加好友',
            error: e
        })
    })
})

app.delete('/friend', function(req, res, next) {
    var user__friend = req.body
    console.log('删除用户', user__friend)
    knex('user__friend').where(user__friend).del().then(function(n) {
        res.status(200).json({
            affected_rows: n
        })
    })
})


app.post('/rooms', function(req, res, next) {
    var _room = req.body.room
    var _creator = req.body.creator
    // var ur = {
    //     user_id: _creator.id,
    //     room_id: _room.id
    // }
    new Room(_room).save()
        .then(function(room) {
            // new User(_creator).save().then(function(user) {
            //     res.status(200).json({
            //         room: room,
            //         creator: user
            //     })
            // })
            knex('user__room').insert({ user_id: _creator.id, room_id: room.id }).then(function(response) {
                res.status(200).json({
                    message: '创建房间成功',
                    room: room,
                    // response: response
                    creator: _creator
                })
            })
        })
        // .then(function(user) {
        //     new User(_creator).save()
        // })
})

app.get('/rooms', function(req, res, next) {
    var uid = req.query.uid
    // knex('user__room').where({ user_id: uid }).then(function(rooms) {

    // })
    // new User({ id: uid }).rooms()
    
    // console.log('uid', uid)
    // new Room().fetch().then(function(rooms) {

    // })
})

app.delete('/rooms', function(req, res, next) {
    // knex('user__room').where
    // var deleteRoom = {
    //     room_id: ,
    //     user_id: this.$store.state.user.id,
    // }
    var deleteRoom = req.body
    console.log('delete room', deleteRoom)
    knex('user__room').where(deleteRoom).del().then(function(response) {
        console.log('delete ok', response)
        res.status(200).json({
            message: '成功退出房间',
            user__room: deleteRoom
        })
    })
})

app.get('/rooms/:room_id', function(req, res, next) {
    var room_id = req.params.room_id
    new Room({ id: room_id }).fetch({ withRelated: ['members'] }).then(function(room) {
        res.status(200).json(room)
    })
})

app.post('/join', function(req, res, next) {
    var user__room = req.body
    // console.log('user__room', user__room)
    knex('user__room').insert(user__room).then(function(ur) {
        res.status(200).json({
            message: `${ur.user_id}号用户成功加入${ur.room_id}号房间`,
            ur: ur
        })
    })
    .catch(function(e) {
        // console.error(e)
        console.error('您已加入该房间，不能重复加入')
    })
})

app.get('/avatars/:uid', function(req, res, next) {
    var uid = req.params.uid
    // console.log('uid', uid)
    // new User({ id: uid }).fetch().then(function(user) {
    //     res.status(200).json({

    //     })
    // })
    res.status(404).end('Image Not Found')
})

app.post('/users/avatar', function(req, res, next) {
    // console.log('body', req.body)
    var image = req.body.image
    var uid = req.body.uid
    // var base = new Base64()
    // console.log('base', base)
    // console.log('image', image)
    var buffer = new Buffer(image, 'base64')
    // console.log('buffer', buffer)
    var filename = `IMG_${uid}`
    fs.writeFileSync('./public/avatars/' + filename, buffer)
    res.status(200).json({
        filename: filename
    })
    new User({ id: uid }).save({ avatar: filename })
})


server.listen(8081, function() {
    console.log('API服务器正在监听 8081 端口')
})