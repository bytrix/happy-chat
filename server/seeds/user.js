
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('user').del()
    .then(function () {
      // Inserts seed entries
      return knex('user').insert([
        {id: 1, username: 'jack', password: '123456', phone: '18149542076'},
        {id: 2, username: 'tom', password: '123456', phone: '18149542077'}
      ]);
    });
};
