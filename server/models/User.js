var bookshelf = require('./bookshelf')
var Room = require('./Room')
// var knex = require('./knex')

var User = bookshelf.Model.extend({
    tableName: 'user',
    friends: function() {
        return this.belongsToMany(User, 'user__friend', 'user_id', 'friend_id')
    },
    // friends_reverse: function() {
    //     return this.belongsToMany(User, 'user__friend', 'friend_id', 'user_id')
    // },
    new_friends: async function() {
        var uid = this.toJSON().id
        var sql_1 = `SELECT friend_id FROM user__friend WHERE user_id = ${uid}`
        var sql_2 = `SELECT user_id FROM user__friend WHERE friend_id = ${uid} AND user_id NOT IN (${sql_1})`
        var sql_3 = `SELECT * FROM user WHERE id IN (${sql_2})`
        
        // console.log(sql_2)
        // const response_2 = await bookshelf.knex.raw(sql_2)
        // console.log(response_2[0])
        
        // console.log(sql_3)
        const response_3 = await bookshelf.knex.raw(sql_3)
        // console.log(response_3[0])
        // return JSON.stringify(response[0])
        return JSON.stringify(response_3[0])

        // console.log('---')
        // var _this = this
        // console.log(bookshelf.knex.raw('SELECT * FROM user'))
        // async function f() {
        //     // console.log('----')
        //     const response = await bookshelf.knex.raw('SELECT * FROM user')
        //     // console.log('----')
        //     // console.log(JSON.stringify(response[0]))
        //     // console.log('----')
        //     const d = JSON.stringify(response[0])
        //     return d
        //     // return response
        // }

        // const a = f()
        // // console.log(a)
        // console.log(a.length)
        // f()
        // return this.belongsToMany(User, 'user__friend', 'friend_id', 'user_id')
        // return r
        // console.log(this)
        // return this.query().select(bookshelf.knex.raw('SELECT * FROM user'))
        // return this.parse(bookshelf.knex.raw('SELECT * FROM user'))
        // this.query(function(qb) {
        //     console.log('---')
        //     var q = qb.column(qb.knex.raw('SELECT * FROM user'))
        //     console.log(q)
        //     console.log('---')
        // })

        // return this.belongsToMany(User, 'user__friend', 'friend_id', 'user_id')

        // return 'aa'
    },
    rooms: function() {
        return this.belongsToMany(Room, 'user__room', 'user_id', 'room_id')
    }
})

// module.exports = User
export default User