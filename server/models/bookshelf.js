var knex_config = require('../knexfile')['development']
var knex = require('knex')(knex_config)
var bookshelf = require('bookshelf')(knex)

module.exports = bookshelf