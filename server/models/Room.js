var bookshelf = require('./bookshelf')
// var User = require('./User')
// 使用require会导致循环引用，使用ES6的import关键字解决
import User from './User'

var Room = bookshelf.Model.extend({
    tableName: 'room',
    members: function() {
        return this.belongsToMany(User, 'user__room', 'room_id', 'user_id')
    }
})

module.exports = Room