
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user__friend', function(table) {
    table.integer('user_id')
    table.integer('friend_id')
    table.primary(['user_id', 'friend_id'])
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user__friend')
};
