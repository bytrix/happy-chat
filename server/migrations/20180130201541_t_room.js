
exports.up = function(knex, Promise) {
  return knex.schema.createTable('room', function(table) {
      table.increments('id')
      table.string('name')
      table.string('icon')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('room')
};
