
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user', function(table) {
      table.increments('id')
      table.string('username').notNullable().unique()
      table.string('password').notNullable()
      table.string('phone', 11).unique()
      table.string('avatar')
      table.date('join_at')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user')
};
