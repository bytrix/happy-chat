
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user__room', function(table) {
      table.integer('user_id')
      table.integer('room_id')
      table.primary(['user_id', 'room_id'])
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user__room')
};
