#!/bin/bash
mysql -uroot -proot<<EOF

DROP DATABASE IF EXISTS happy_chat;
CREATE DATABASE happy_chat;

EOF

knex migrate:rollback
knex migrate:latest
