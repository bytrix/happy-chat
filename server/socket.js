var https = require('https')
// var http = require('http')
var fs = require('fs')
var server = https.createServer({
    key: fs.readFileSync('./ssl/key.pem'),
    cert: fs.readFileSync('./ssl/server.crt')
})
// var server = http.createServer()
var io = require('socket.io')(server)
var axios = require('axios')
// import env from '../env/default'
var redis = require('redis')
var env = require('../env/default')

const REDIST_HOST = '127.0.0.1'

var redisClient = redis.createClient(6379, REDIST_HOST)

redisClient.on('connect', function(err) {
    console.log('已连接Redis缓存')
})

// var clients = []
// var client = {
//     uid: 0,
//     sid: null
// }


const axiosRequestConfig = {
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
}

var getSocketByUserId = function(uid) {
    var socket = Object.values(io.sockets.sockets).filter(function(s) {
        return s.uid == uid
    })[0]
    return socket
}

io.on('connection', function(socket) {
    // var uids = []
    // console.log(io.sockets.sockets)
    // client_sid = socket.id
    // clients.push(client_sid)
    // console.log(socket.handshake.query)
    var uid = socket.handshake.query['uid']
    // console.log('uid', uid)
    // console.log('uid == null', uid == 'null')
    // if(uid == null) {
    //     return
    // }
    if(uid == 'null') {
        return
    }
    socket.uid = uid
    console.log('\n-------------- 当前已登录的客户端 ------------------\n')
    for(s in io.sockets.sockets) {
        console.log(`socket_id: %s\t\tuser_id: %d`, s, io.sockets.sockets[s].uid)
        // uids.push(io.sockets.sockets[s].uid)
    }
    io.emit('onlineUsers', Object.values(io.sockets.sockets).map( (socket) => (socket.uid) ))
    // console.log('uids', uids)

    // redisClient.get(uid, function(err, message) {
    //     if(message != null) {
    //         console.log(`${uid}号用户有新消息`)
    //         socket.emit('receive', message)
    //         redisClient.del(uid)
    //     }
    // })
    redisClient.lrange(uid, 0, -1, function(err, messages) {
        if(messages != null && messages.length != 0) {
            // console.log(messages)
            // messages = JSON.parse(messages)
            console.log(`${uid}号用户有${messages.length}条新消息`)
            // for(message in messages) {
            //     socket.emit('receive', JSON.parse(message))
            // }
            messages.forEach(function(message) {
                socket.emit('receive', JSON.parse(message))
            })
            redisClient.del(uid)
        }
    })
    // console.log('---- connection ----')
    
    // console.log('new user...')
    // console.log(clients)

    // socket.on('login', function(uid) {
    //     // client.uid = uid
    //     // client.sid = socket.id
    //     // clients.push(client)
    //     // console.log(clients)
    //     console.log('---- login ----')
    //     socket.uid = uid
    //     for(s in io.sockets.sockets) {
    //         console.log(s, io.sockets.sockets[s].id, io.sockets.sockets[s].uid)
    //     }
    //     console.log('---- login ----')
    // })

    // 监听来自客户端的消息
    socket.on('privateSend', function(message) {
        console.log('privateSend: ', message)
        // 从clients数组中获取与message.to_uid对应的client
        // var c = clients.filter(function(c) {
        //     if(c.uid == message.to_uid) {
        //         return true
        //     }
        // })
        // var target_socket = Object.values(io.sockets.sockets).filter(function(s) {
        //     // console.log('filter...')
        //     // console.log(s.uid)
        //     return s.uid == message.to_u.id
        // })[0]
        // var source_socket = Object.values(io.sockets.sockets).filter(function(s) {
        //     // console.log('filter...')
        //     // console.log(s.uid)
        //     return s.uid == message.from_u.id
        // })[0]

        // var target_socket = io.sockets.sockets[message.to_u.id]
        // var source_socket = io.sockets.sockets[message.from_u.id]
        var target_socket = getSocketByUserId(message.to_u.id)
        var source_socket = getSocketByUserId(message.from_u.id)

        // console.log(clients[c].sid)
        // 转发消息
        // console.log('sockets: ', io.sockets.sockets)
        // console.log('target: ', target_socket)
        // console.log('source: ', target_socket)
        if(target_socket != undefined) {
            target_socket.emit('receive', message)
        } else {
            // redisClient.set(`${message.to_u.id}`, JSON.stringify(message))
            redisClient.rpush(`${message.to_u.id}`, JSON.stringify(message))
            console.log(`目标用户[${message.to_u.id}]未上线，消息将被存储在Redis缓存`)
        }
        
        source_socket.emit('receive', message)
        // console.log('emitted')
        io.emit('debug', 'this is debug info')
    })

    socket.on('publicSend', function(message) {
        console.log('publicSend', message)
        // console.log('map', message.to_u.members.map(member => member.id))
        var ids = message.to_u.members.map(member => member.id)
        // console.log('ids', ids)
        // ids.forEach(function(id) {
        //     console.log('id', id, typeof id)
        // })
        var target_sockets = Object.values(io.sockets.sockets).filter(function(s) {
            // return s.uid ==
            // console.log('s.uid', s.uid)
            // console.log('indexOf', ids.indexOf(s.uid))
            // console.log('type of s.uid', typeof s.uid)
            return ids.indexOf(Number(s.uid)) != -1
        })
        // console.log('target_sockets', target_sockets)
        target_sockets.forEach(function(target_socket) {
            target_socket.emit('receive', message)
        })
    })

    socket.on('apply', function(user__friend) {
        console.log('add friend')
        console.log(user__friend)
        axios.post(env.API_HOST + '/friend', user__friend, axiosRequestConfig).then(function(response) {
            console.log('data', response.data)
            // console.log('socket', socket)
            // var target_socket = Object.values(io.sockets.sockets).filter(function(s) {
            //     // console.log('filter...')
            //     // console.log(s.uid)
            //     return s.uid == user__friend.friend_id
            // })[0]
            var target_socket = getSocketByUserId(user__friend.friend_id)
            // console.log('target_socket', target_socket)
            // console.log('socket', socket)
            if(target_socket == undefined) {
                console.log(`%d号用户未上线，系统无法通知对方！`, user__friend.friend_id)
            } else {
                target_socket.emit('notify', response.data)
            }
            socket.emit('apply_feedback', response.data)
        })
        .catch(function(e) {
            // console.log('error', e)
            console.log('HELLO!!')
        })
    })

    socket.on('agree', function(user__friend) {
        console.log('agree: user_friend', user__friend)
        axios.post(env.API_HOST + '/friend', user__friend, axiosRequestConfig).then(function(response) {
            
            // user__friend.user_id

            // var target_socket = Object.values(io.sockets.sockets).filter(function(s) {
            //     return s.uid == user__friend.friend_id
            // })[0]
            var target_socket = getSocketByUserId(user__friend.friend_id)
            // console.log('target_socket', target_socket)
            target_socket.emit('agree_feedback', response.data)
        })
    })

    socket.on('disconnect', function() {
        // clients.splice(clients.indexOf(client), 1)
        io.emit('onlineUsers', Object.values(io.sockets.sockets).map( (socket) => (socket.uid) ))

        console.log('\n-------------- 当前已登录的客户端 ------------------\n')
        for(s in io.sockets.sockets) {
            console.log(`socket_id: %s\t\tuser_id: %d`, s, io.sockets.sockets[s].uid)
        }
    })







    // --------------------------------------- 视频通信部分 ---------------------------------------
    // var ids = []

    socket.on('inviteVideoChat', function(packet) {
        console.log('inviteVideoChat')
        // console.log(packet)
        var target_socket = getSocketByUserId(packet.peer.friend.id)
        target_socket.emit('onInviteVideoChat', packet.peer)
    })

    socket.on('joinVideoChat', function(packet) {
        // ids.push(packet.peer.user_id)
        console.log('%d joinVideoChat', packet.peer.user.id)
        console.log('%d joinVideoChat', packet.peer.friend.id)
        // console.log('ids', ids)
        // console.log(packet)
        // for(var i = 0; i < packet.user_id.length; i++) {
        //     console.log(i, packet.user_id[i])
        //     this.$socket.emit('createOffer', {
        //         user_id: packet.user_id[i]
        //     })
        // }
        var target_socket = getSocketByUserId(packet.peer.friend.id)
        var self_socket = getSocketByUserId(packet.peer.user.id)
        target_socket.emit('createOffer', packet)
        // target_socket.emit('createOffer', {
        //     peer: {
        //         user: packet.peer.friend,
        //         friend: packet.peer.user
        //     }
        // })
        self_socket.emit('createOffer', packet)
        // self_socket.emit('createOffer', {
        //     peer: {
        //         user: packet.peer.friend,
        //         friend: packet.peer.user
        //     }
        // })
        // for(var i = 0; i < ids.length; i++) {
        //     var socket = getSocketByUserId(ids[i])
        //     socket.emit('createOffer')
        // }
    })

    socket.on('createOffer', function(packet) {
        // console.log('%d createOffer', packet.peer.user.id)
        console.log('createOffer')
        console.log(packet)
        var self_socket = getSocketByUserId(packet.peer.user.id)
        // console.log('peer', packet.peer)
        var target_socket = getSocketByUserId(packet.peer.friend.id)

        target_socket.emit('receiveOffer', packet)
        // target_socket.emit('receiveOffer', {
        //     peer: {
        //         user: packet.peer.friend,
        //         friend: packet.peer.user
        //     },
        //     data: packet.data
        // })
        // self_socket.emit('receiveOffer', packet)
        self_socket.emit('receiveOffer', {
            peer: {
                user: packet.peer.friend,
                friend: packet.peer.user
            },
            data: packet.data
        })
        
    })

    socket.on('receiveAnswer', function(packet) {
        console.log('%d receiveAnswer', packet.peer.user.id)
        // console.log(packet)
        var target_socket = getSocketByUserId(packet.peer.user.id)
        target_socket.emit('receiveAnswer', packet)
    })

    socket.on('createCandidate', function(packet) {
        if(packet.data == null)
            return
        console.log('%d createCandidate', packet.peer.user.id)
        // console.log(packet)
        var target_socket = getSocketByUserId(packet.peer.user.id)
        target_socket.emit('receiveCandidate', packet)
    })


    // 文件传输部分

    socket.on('fileTransferAsk', function(packet) {
        // console.log('fileTransferAsk', packet.file, packet.user.id)
        console.log('fileTransferAsk', packet)
        var target_socket = getSocketByUserId(packet.to_u.id)
        target_socket.emit('onFileTransferAsk', packet)
    })

    socket.on('fileTransferAskFeedback', function(packet) {
        console.log('fileTransferAskFeedback', packet)
        var target_socket = getSocketByUserId(packet.from_u.id)
        target_socket.emit('fileTransferAskFeedback', packet)
    })



})

server.listen(3001, function() {
    console.log('Socket服务器正在监听 3001 端口')
})
