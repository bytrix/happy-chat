# happy-chat（毕设项目）

## 图片展示在未登录环境下看不到，可点击readme-assets文件夹中查看

### 运行环境

- Node.js 9.x
- MySQL
- Redis
- Knex.js
- Bookshelf.js
- Babel CLI

----------

### 部署时可能需要修改的地方

修改文件|修改处|备注
-|-|-
webpack.config.js|devServer.host|开发服务器
env/default.js|API_HOST / SOCKET_HOST|接口服务器 / Socket服务器
src/assets/scss/emoji-sprite.scss|STATIC_HOST|资源服务器
server/socket.js|REDIS_HOST|缓存服务器
package.json|KEY / CERT|SSL证书
server/knexfile.js||数据库信息


----------

### 使用HTTPS运行Vue项目

创建私钥和证书

    openssl req -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
    openssl x509 -req -days 365 -in csr.pem -signkey key.pem -out server.crt

配置Webpack

    webpack-dev-server --https --key KEY --cert CERT

### Demo

#### 私聊

![private chat](https://gitee.com/bytrix/happy-chat/blob/master/readme-assets/private_chat_3_without_spotlight.gif)

#### 群聊

![private chat](https://gitee.com/bytrix/happy-chat/blob/master/readme-assets/room_chat_a_without_spotlight.gif)
![private chat](https://gitee.com/bytrix/happy-chat/raw/master/readme-assets/room_chat_b_without_spotlight.gif)

#### 文件传输

![private chat](https://gitee.com/bytrix/happy-chat/blob/master/readme-assets/file_transfer_b.gif)

#### 视频聊天

![private chat](https://gitee.com/bytrix/happy-chat/blob/master/readme-assets/video_chat.gif)
